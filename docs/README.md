---
home: true
heroImage: /images/hero.png
heroText: DGDDI, Data, Documentation
actionText: Consulter les guides →
actionLink: /accueil
tagline: toute la documentation
footer: MIT Licensed | Copyright © 2018-present Evan You
---