module.exports = {
   title: 'POC site central doc on GitLab + VuePress',
    description: 'Vue-powered static site generator running on GitLab Pages',
    base: '/vuepress-poc2.io/',
    dest: 'public',
    themeConfig: {
        logo: '/images/logo-marianne.svg',
        theme: 'vuepress-theme-gouv-fr',
        nav: [
            { text: 'Home', link: '/' },
            { text: 'Tout les guides', link: '/accueil'}
            
        ],
        displayAllHeaders: true,
        
        sidebarDepth: 2,
        sidebar: {
            '/vision360/' : ['','back','front','preprocessing'],

            '/operation-douaniere/' : ['','back','front','preprocessing'],

            '/mutation/' : ['','back','front','preprocessing'],

            '/deploiment-routier/' : ['','back','front','preprocessing'],

            '/baz' : 'auto',
            //fallback
            '/':['']
        }
    }
};